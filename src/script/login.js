const email = document.querySelector(".email");
const pass = document.querySelector(".pass");
const confPass = document.querySelector(".confpass");
const submit = document.querySelector(".submit-button");
const emailError = document.querySelector(".emailError");
const passError = document.querySelector(".passError");
const confPassError = document.querySelector(".confPassError");

const verifEmail = function (email) {
  return !(
    email !== "" &&
    email.indexOf("@") !== -1 &&
    email.indexOf(".") !== -1
  );
};

const verifPassword = function (pass) {
  return !(pass.length >= 8 && pass !== "");
};

const verifConfPasword = function (pass1, pass2) {
  return !(pass1 !== "" && pass1 === pass2);
};

submit.addEventListener("click", function (e) {
  e.preventDefault();
  if (verifEmail(email.value)) {
    email.style.backgroundColor = "rgba(255, 192, 192, 0.807)";
    emailError.innerHTML = "Email is required";
  } else {
    email.style.backgroundColor = "white";
    emailError.innerHTML = "";
  }

  if (verifPassword(pass.value)) {
    pass.style.backgroundColor = "rgba(255, 192, 192, 0.807)";
    passError.innerHTML = "Passowrd is required";
  } else {
    pass.style.backgroundColor = "white";
    passError.innerHTML = "";
  }

  if (verifConfPasword(confPass.value, pass.value)) {
    confPass.style.backgroundColor = "rgba(255, 192, 192, 0.807)";
    confPassError.innerHTML = "Confirm passowrd is required";
  } else {
    confPass.style.backgroundColor = "white";
    confPassError.innerHTML = "";
  }
});
